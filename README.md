# [www.tipngo.ru](http://tipngo.ru) - register and get Tip-code #
# Tip&Go - an app to pay tips without cash #

### Real appstore project! AppStore link: [https://appsto.re/ru/jevIfb.i](https://appsto.re/ru/jevIfb.i) ###

* used VIPER architecture
* used Yandex Money SDK to make transactions

###           Enter tip-code (test tip-code "30508") ###
![300.jpg](https://bitbucket.org/repo/Kkb7Xg/images/117308662-300.jpg)

###           Enter tip value ###
![3002.jpg](https://bitbucket.org/repo/Kkb7Xg/images/695694411-3002.jpg)

###           Pay from your bank card ###
![3003.jpg](https://bitbucket.org/repo/Kkb7Xg/images/4070287230-3003.jpg)