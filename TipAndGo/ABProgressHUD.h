//
//  ABProgressHUD.h
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABProgressHUD : NSObject

+ (void)show;
+ (void)hide;
+ (BOOL)isVisable;
@end
