//
//  ABInputTipCodeWireframeInput.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ABProfile;

@protocol ABInputTipCodeWireframeInput <NSObject>

- (void)openProfile:(ABProfile*)profile from:(UIViewController*)fromViewController;
- (void)openTermsConditions:(UIViewController*)fromViewController;

@end
