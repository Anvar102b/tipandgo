//
//  ABCommonHelper.m
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABCommonHelper.h"
#import <UIKit/UIKit.h>

@implementation ABCommonHelper

+(ScreenSizeInch)screenSizeInch{
    if ([[UIScreen mainScreen] bounds].size.height < 568)
        return ScreenSize_3_5_inch;
    else if ([[UIScreen mainScreen] bounds].size.height == 568)
        return ScreenSize_4_inch;
    else if ([[UIScreen mainScreen] bounds].size.height == 667.0)
        return ScreenSize_4_7_inch;
    else if ([[UIScreen mainScreen] bounds].size.height == 736.0)
        return ScreenSize_5_5_inch;
    else {
        NSLog(@"            !!! ERROR: screenSizeInch - wrong size");
        return 0;
    }
}

+ (NSString*)cardType:(YMAPaymentCardType)cardType; {
    NSString* cardTypeString;
    switch (cardType) {
        case YMAPaymentCardTypeVISA:
            cardTypeString = @"VISA";
            break;
        case YMAPaymentCardTypeMasterCard:
            cardTypeString = @"MasterCard";
            break;
        case YMAPaymentCardTypeAmericanExpress:
            cardTypeString = @"American Express";
            break;
        case YMAPaymentCardTypeJCB:
            cardTypeString = @"JCB";
            break;
        default:
            cardTypeString = @"Карта";
            break;
    }
    return cardTypeString;
}

+ (NSString*)cardNums:(NSString*)cardNumber {
    NSString* cardNumString;
    
    if (cardNumber.length <6) {
        cardNumString = cardNumber;
    } else {
        cardNumString = [cardNumber substringFromIndex:((cardNumber.length) - 5 )];
    }
    return cardNumString;
}


@end
