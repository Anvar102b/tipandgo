//
//  ABProgressHUD.m
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABProgressHUD.h"
#import <SVProgressHUD.h>

@implementation ABProgressHUD

+ (void)show {
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setForegroundColor:BlueColor];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
}

+ (void)hide {
    [SVProgressHUD dismiss];
}

+ (BOOL)isVisable {
    return [SVProgressHUD isVisible];
}

@end
