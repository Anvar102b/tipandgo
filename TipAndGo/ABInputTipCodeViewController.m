//
//  ABInputTipCodeViewController.m
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABInputTipCodeViewController.h"
#import "ABKeyboardView.h"
#import "ABPinView.h"

#import "ABInputTipCodePresenter.h"

@interface ABInputTipCodeViewController () <ABPinViewDelegate>

@property (weak, nonatomic) IBOutlet ABPinView *pinView;
@property (weak, nonatomic) IBOutlet ABKeyboardView *keyboardView;
@property (weak, nonatomic) IBOutlet UIView* bgView;
@property (weak, nonatomic) IBOutlet UILabel* termsLabel;

@end

@implementation ABInputTipCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initObjects];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
}

#pragma mark - Init Objects

- (void)initObjects {
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.tintColor = [UIColor blackColor];    
    _keyboardView.delegate = _pinView;
    _keyboardView.showDoneButton = NO;
    _pinView.output = _keyboardView;
    _pinView.delegate = self;
    _bgView.center = CGPointMake(CGRectGetMidX(self.view.bounds), CGRectGetMidY(self.view.bounds) - CGRectGetHeight(_bgView.bounds) / 2);
}

#pragma mark - ABInputTipCodeViewInput

- (void)showLoadingAnimation {
    BeginIgnoringInteractionEvents;
    [_pinView startAnimation];
}

- (void)hideLoadingAnimation {
    [_pinView stopAnimation];
    EndIgnoringInteractionEvents;
}

- (void)showPinShake {
    [_pinView shake:_pinView];
}

- (void)showErrorMessage:(NSString*)errorMessage {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:errorMessage preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction* okbtn = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okbtn];
        [self presentViewController:alert animated:YES completion:nil];
    });
}

#pragma mark - ABPinViewDelegate

- (void)didEnterTipCode:(NSString*)tipCode {
    [_presenter obtainProfile:tipCode];
}

#pragma mark - Actions 

- (IBAction)openTermsConditions:(UIButton*)sender {
    [_presenter openTermsConditions];
}


@end
