//
//  ABInputTipCodeViewInput.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABInputTipCodeViewInput <NSObject>

- (void)showLoadingAnimation;
- (void)hideLoadingAnimation;
- (void)showPinShake;
- (void)showErrorMessage:(NSString*)errorMessage;

@end
