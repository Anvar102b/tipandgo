//
//  ABYandexMoneyBackend.m
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABYandexMoneyBackend.h"
#import "YMAExternalPaymentSession.h"
#import "YMAExternalPaymentRequest.h"
#import "YMAProcessExternalPaymentRequest.h"

#import "ABKeyChainHandler.h"

static NSString *const kSuccessUrl = @"yandexmoneyapp:oauth/authorize/success";
static NSString *const kFailUrl = @"yandexmoneyapp:oauth/authorize/fail";
static NSString *const kClientId = @"9748720F59A12110ED0CC67EA63892A10B85EAC29D7A3FB6EB91B6EF32883B15";

@interface ABYandexMoneyBackend () {
    YMAExternalPaymentSession *_session;
}

@property (nonatomic, strong) YMAExternalPaymentSession *session;
@property (nonatomic, strong) YMAExternalPaymentInfoModel *paymentRequestInfo;
@property (nonatomic, strong) YMAMoneySourceModel* sourceModel;

@end

@implementation ABYandexMoneyBackend

+ (ABYandexMoneyBackend*) sharedManager {
    
    static ABYandexMoneyBackend* manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(
                  &onceToken, ^{
                      manager = [[ABYandexMoneyBackend alloc]init];
                  });
    return manager;
}


- (NSArray*)savedCards {
    return [ABKeyChainHandler getSavedCardsInfo];
}

- (YMAExternalPaymentSession *)session {
    if (!_session) {
        _session = [[YMAExternalPaymentSession alloc] init];
    }
    return _session;
}

- (void)checkForInstanceId:(void(^)())success
                   failure:(void(^)(ABError* error))failure {
    NSString* savedInstanceId = [ABKeyChainHandler getInstanceId];
    if (savedInstanceId == nil || [savedInstanceId isEqual:@""]) {
        [self.session instanceWithClientId:kClientId token:nil completion:^(NSString *instanceId, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (instanceId) {
                    if ([ABKeyChainHandler saveInstanceId:instanceId]) {
                        NSLog(@"saveInstanceId %@", instanceId);
                    }
                    self.session.instanceId = instanceId;
                    success();
                } else {
                    ABError* errorObject = [[ABError alloc] initWithText:error.localizedDescription];
                    failure(errorObject);
                }
            });
        }];
        return;
    }
    self.session.instanceId = savedInstanceId;
    success(savedInstanceId);
}

- (void)paymentRequestWallet:(NSString*)wallet
                   tipAmount:(NSString*)tipAmmount
                     success:(void(^)())success
                     failure:(void(^)(ABError* error))failure {
    NSDictionary* paymentParams = @{@"pattern_id" : @"p2p",
                                    @"instance_id" : self.session.instanceId,
                                    @"to" : wallet,
                                    @"amount" : tipAmmount,
                                    @"message" : @"Tip&Go Payment"};
    YMABaseRequest *externalPaymentRequest = [YMAExternalPaymentRequest externalPaymentWithPatternId:@"p2p" paymentParameters:paymentParams];
    [self.session performRequest:externalPaymentRequest
                           token:nil
                      completion:^(YMABaseRequest *request, YMABaseResponse *response, NSError *error) {
                          dispatch_async(dispatch_get_main_queue(), ^{
                              if (error) {
                                  ABError* errorObject = [[ABError alloc] initWithText:error.localizedDescription];
                                  failure(errorObject);
                              } else {
                                  YMAExternalPaymentResponse *externalPaymentResponse = (YMAExternalPaymentResponse *) response;
                                  self.paymentRequestInfo = externalPaymentResponse.paymentRequestInfo;
                                  success();
                              }
                          });
                      }];
}

- (void)processPaymentRequestAskMoneySource:(void(^)())success
                               authRequired:(void(^)(YMAAscModel *asc))authRequired
                                    failure:(void(^)(ABError* error))failure {
        YMABaseRequest *paymentRequest = [YMAProcessExternalPaymentRequest processExternalPaymentWithRequestId:self.paymentRequestInfo.requestId successUri:kSuccessUrl failUri:kFailUrl requestToken:YES];
    [self.session performRequest:paymentRequest token:nil completion:^(YMABaseRequest *request, YMABaseResponse *response, NSError *error) {
        YMABaseProcessResponse *processResponse = (YMABaseProcessResponse *)response;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                ABError* errorObject = [[ABError alloc] initWithText:error.localizedDescription];
                failure(errorObject);
            } else if (processResponse.status == YMAResponseStatusInProgress) {
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, processResponse.nextRetry);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [self processPaymentRequestAskMoneySource:^{} authRequired:^(YMAAscModel *asc) {} failure:^(ABError *error) {}];
                });
            } else {
                YMAProcessExternalPaymentResponse *processExternalPaymentResponse = (YMAProcessExternalPaymentResponse *) response;
                YMAAscModel *asc = processExternalPaymentResponse.asc;
                YMABaseProcessResponse *processResponse = (YMABaseProcessResponse *)response;
                if (processResponse.status == YMAResponseStatusSuccess) {
                    _sourceModel = processExternalPaymentResponse.moneySource;
                    if (processExternalPaymentResponse.moneySource)
                        [ABKeyChainHandler saveUserCardInfo:_sourceModel];

                    success(nil);
                } else if (processResponse.status == YMAResponseStatusExtAuthRequired) {
                    authRequired(asc);
                } else{
                    ABError* errorObject = [[ABError alloc] initWithText:@"Неизвестная ошибка"];
                    failure(errorObject);
                }
            }
        });
    }];
}

- (void)processPaymentRequestWithCardInfo:(YMAMoneySourceModel*)cardInfo
                                      csc:(NSString*)csc
                                  success:(void(^)())success
                 authRequired:(void(^)(YMAAscModel *asc))authRequired
                      failure:(void(^)(ABError* error))failure {
    YMABaseRequest *paymentRequest = [YMAProcessExternalPaymentRequest processExternalPaymentWithRequestId:self.paymentRequestInfo.requestId successUri:kSuccessUrl failUri:kFailUrl moneySourceToken:cardInfo.moneySourceToken csc:csc];
    
    [self.session performRequest:paymentRequest token:nil completion:^(YMABaseRequest *request, YMABaseResponse *response, NSError *error) {
        YMABaseProcessResponse *processResponse = (YMABaseProcessResponse *)response;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (error) {
                ABError* errorObject = [[ABError alloc] initWithText:error.localizedDescription];
                failure(errorObject);
            } else if (processResponse.status == YMAResponseStatusInProgress) {
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, processResponse.nextRetry);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
                    [self processPaymentRequestWithCardInfo:cardInfo csc:csc success:^{} authRequired:^(YMAAscModel *asc) {} failure:^(ABError *error) {}];
                });
            } else {
                YMABaseProcessResponse *processResponse = (YMABaseProcessResponse *)response;
                if (processResponse.status == YMAResponseStatusSuccess) {
                    success(nil);
                } else if (processResponse.status == YMAResponseStatusExtAuthRequired) {
                    YMAProcessExternalPaymentResponse *processExternalPaymentResponse = (YMAProcessExternalPaymentResponse *) response;
                    YMAAscModel *asc = processExternalPaymentResponse.asc;
                    authRequired(asc);
                } else{
                    ABError* errorObject = [[ABError alloc] initWithText:@"Неизвестная ошибка"];
                    failure(errorObject);
                }
            }
        });
    }];
}

- (BOOL)askToSaveCardInfo {
    BOOL ask = NO;
    NSArray* cardsInfoArray = [ABKeyChainHandler getSavedCardsInfo];
    if (cardsInfoArray == nil || cardsInfoArray.count == 0) {
        ask = YES;
    }
    NSMutableArray* sourceTokens = [NSMutableArray array];
    for (NSInteger i = 0; cardsInfoArray.count; i++) {
        YMAMoneySourceModel* cardInfo = cardsInfoArray[0];
        [sourceTokens addObject:cardInfo.moneySourceToken];
    }
    if ([sourceTokens containsObject:_sourceModel.moneySourceToken] == NO) {
        ask = YES;
    }
    return ask;
}

- (void)saveCardInfo {
    [ABKeyChainHandler saveUserCardInfo:_sourceModel];
}

@end
