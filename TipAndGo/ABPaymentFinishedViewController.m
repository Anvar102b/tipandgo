//
//  ABPaymentFinishedViewController.m
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABPaymentFinishedViewController.h"
#import "ABProfile.h"

@interface ABPaymentFinishedViewController ()

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (nonatomic, strong) ABProfile* profile;

@end

@implementation ABPaymentFinishedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_profile.avatarImage) {
        self.avatarImage.image = _profile.avatarImage;
    }
    self.avatarImage.layer.cornerRadius = CGRectGetHeight(self.avatarImage.bounds) / 2.f;
    self.avatarImage.layer.masksToBounds = YES;
    _bgView.backgroundColor = [UIColor clearColor];
//    _bgView.center = self.view.center;
    self.doneButton.layer.cornerRadius = CGRectGetHeight(self.doneButton.bounds) / 2.f;
    self.doneButton.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
}

- (void)setProfile:(ABProfile*)profile {
    _profile = profile;
}

- (IBAction)doneAction:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


@end
