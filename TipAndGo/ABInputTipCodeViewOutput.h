//
//  ABInputTipCodeViewOutput.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABInputTipCodeViewOutput <NSObject>

- (void)obtainProfile:(NSString*)tipCode;
- (void)openTermsConditions;

@end
