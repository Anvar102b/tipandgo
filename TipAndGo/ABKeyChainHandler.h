//
//  ABKeyChainHandler.h
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YMAMoneySourceModel;

@interface ABKeyChainHandler : NSObject

+ (BOOL)saveInstanceId:(NSString*)instanceId;
+ (NSString*)getInstanceId;
+ (BOOL)saveUserCardInfo:(YMAMoneySourceModel*)cardInfo;
+ (NSArray*)getSavedCardsInfo;

@end
