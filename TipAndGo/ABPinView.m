//
//  ABPinView.m
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABPinView.h"

@interface ABPinView ()

@property (nonatomic, strong) NSMutableString* symbolsString;

@property (nonatomic, assign) BOOL isAnimating;

@end

@implementation ABPinView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    _symbolsString = [[NSMutableString alloc] initWithCapacity:0];
    if (self) {
        if (self.subviews.count == 0) {
            UINib *nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
            UIView *subview = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            subview.frame = self.bounds;
            [self addSubview:subview];
        }
    }
    return self;
}

#pragma mark - ABKeyboardViewDelegate

- (void)didpressNum:(NSInteger)number {
    if (_symbolsString.length == 5)
        return;
    
    [_output enableDelete];
    NSString* symbolString = [NSString stringWithFormat:@"%ld", (long)number];
    [_symbolsString appendString:symbolString];
    
    UIView* pin = _numPins[_symbolsString.length - 1];
    pin.hidden = YES;
    
    UILabel* numText = _numLabels[_symbolsString.length - 1];
    numText.hidden = NO;
    numText.text = symbolString;
    
    if (_symbolsString.length == 5)
        [_delegate didEnterTipCode:_symbolsString];
}

- (void)didpressDelete {
    if (_symbolsString.length == 0)
        return;
    _symbolsString.length == 1 ? ([_output disableDelete]) : NO;
    UIView* pin = _numPins[_symbolsString.length - 1];
    pin.hidden = NO;
    UILabel* numText = _numLabels[_symbolsString.length - 1];
    numText.hidden = YES;
    [_symbolsString deleteCharactersInRange:NSMakeRange(_symbolsString.length-1, 1)];
}

#pragma mark - Public Methods

- (void)startAnimation {
    _isAnimating = YES;
    [self performAnimation];
}

- (void)performAnimation {
    if (_isAnimating) {
        for (NSInteger i = 0; i < _numLabels.count; i++) {
            UILabel* label = _numLabels[i];
            [UIView animateWithDuration:0.1*(i+1) animations:^{
                label.alpha = 0.2f;
            } completion:^(BOOL finished) {
                if (i == _numLabels.count-1) {
                    for (NSInteger h = 0; h < _numLabels.count; h++) {
                        UILabel* label = _numLabels[h];
                        [UIView animateWithDuration:0.23*(h+1) animations:^{
                            label.alpha = 1.0f;
                        } completion:^(BOOL finished) {
                            if (h == _numLabels.count-1){
                                if (_isAnimating)
                                    [self startAnimation];
                            }
                        }];
                        
                    }
                }
            }];
        }
    }
}

- (void)stopAnimation {
    [_symbolsString deleteCharactersInRange:NSMakeRange(0, _symbolsString.length)];
    
    for (UILabel* label in _numLabels) {
        label.text = @"";
        label.hidden = YES;
    }
    
    for (UIView* pins in _numPins) {
        pins.hidden = NO;
    }
    _isAnimating = NO;
}

- (void)shake:(UIView*)pinView {
    [_output disableDelete];
    CAKeyframeAnimation* animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values = @[@0, @10, @-10, @10, @0];
    animation.duration = 0.3f;
    animation.additive = YES;
    [pinView.layer addAnimation:animation forKey:@"shake"];
}

@end
