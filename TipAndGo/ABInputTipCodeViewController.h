//
//  ABInputTipCodeViewController.h
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABInputTipCodeViewInput.h"

@protocol ABInputTipCodeViewOutput;

@interface ABInputTipCodeViewController : UIViewController <ABInputTipCodeViewInput>

@property (nonatomic, strong) id <ABInputTipCodeViewOutput> presenter;

@end
