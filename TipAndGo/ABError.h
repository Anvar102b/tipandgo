//
//  ABError.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    WrongCode = 1,
} ABExceptionType;

@interface ABError : NSObject

@property (nonatomic, strong) NSString* text;
@property (nonatomic, assign) ABExceptionType exceptionType;
@property (nonatomic, strong) NSString* ticketId;

- (instancetype)initWithText:(NSString*)text exceptionType:(NSString*)type ticketId:(NSString*)ticketId;
- (instancetype)initWithText:(NSString*)text;


 
@end
