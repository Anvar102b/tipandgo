//
//  ABInputTipCodePresenter.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABInputTipCodePresenter.h"
#import "ABInputTipCodeViewController.h"
#import "ABInputTipCodeInteractor.h"
#import "ABInputTipCodeWireframe.h"

#import "ABProfile.h"

@implementation ABInputTipCodePresenter

#pragma mark - ABInputTipCodeViewOutput

- (void)obtainProfile:(NSString*)tipCode {
    [_view showLoadingAnimation];
    [_interactor loadProfile:tipCode];
}

- (void)openTermsConditions {
    [_wireframe openTermsConditions:(ABInputTipCodeViewController*)_view];
}

#pragma mark - ABInputTipCodeInteractorOutput

- (void)error:(NSString*)errorText {
    [_view hideLoadingAnimation];
    [_view showPinShake];
    [_view showErrorMessage:errorText];
}

- (void)didLoadProfile:(ABProfile*)profile {
    [_view hideLoadingAnimation];
    [_wireframe openProfile:profile from:(ABInputTipCodeViewController*)_view];
}

@end
