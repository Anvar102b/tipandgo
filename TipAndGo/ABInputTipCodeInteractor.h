//
//  ABInputTipCodeInteractor.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABInputTipCodeInteractorInput.h"

@protocol ABInputTipCodeInteractorOutput;

@interface ABInputTipCodeInteractor : NSObject <ABInputTipCodeInteractorInput>

@property (nonatomic, weak) id <ABInputTipCodeInteractorOutput> presenter;

@end
