//
//  ABInputTipCodeInteractorOutput.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABProfile;

@protocol ABInputTipCodeInteractorOutput <NSObject>

- (void)error:(NSString*)errorText;
- (void)didLoadProfile:(ABProfile*)profile;

@end
