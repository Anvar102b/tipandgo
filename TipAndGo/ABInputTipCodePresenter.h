//
//  ABInputTipCodePresenter.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABInputTipCodeViewOutput.h"
#import "ABInputTipCodeInteractorOutput.h"

@protocol ABInputTipCodeViewInput;
@protocol ABInputTipCodeInteractorInput;
@protocol ABInputTipCodeWireframeInput;

@interface ABInputTipCodePresenter : NSObject <ABInputTipCodeViewOutput, ABInputTipCodeInteractorOutput>

@property (nonatomic, weak) id <ABInputTipCodeViewInput> view;
@property (nonatomic, strong) id <ABInputTipCodeInteractorInput> interactor;
@property (nonatomic, strong) id <ABInputTipCodeWireframeInput> wireframe;

@end
