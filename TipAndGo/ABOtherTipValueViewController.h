//
//  ABOtherTipValueViewController.h
//  TipAndGo
//
//  Created by Anvar Basharov on 14.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABOtherTipValueViewControllerDelegate <NSObject>

- (void)didEnteredTipValue:(NSString*)tipValue;

@end

@interface ABOtherTipValueViewController : UIViewController

@property (nonatomic, weak) id <ABOtherTipValueViewControllerDelegate> delegate;

@end
