//
//  ABKeyboardView.h
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABPinViewOutput.h"

@protocol ABKeyboardViewDelegate <NSObject>

- (void)didpressNum:(NSInteger)number;
- (void)didpressDelete;
@optional
- (void)didPressDone;

@end

@interface ABKeyboardView : UIView <ABPinViewOutput>

@property (nonatomic, weak) id <ABKeyboardViewDelegate> delegate;
@property (nonatomic, assign) BOOL showDoneButton;

@end
