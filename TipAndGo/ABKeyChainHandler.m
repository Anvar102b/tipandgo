//
//  ABKeyChainHandler.m
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABKeyChainHandler.h"
#import "YMAMoneySourceModel.h"
#import "Lockbox.h"

static NSString* kInstanceId = @"kInstanceId";
static NSString* kCardsInfo = @"kCardsInfo";

@implementation ABKeyChainHandler

+ (BOOL)saveInstanceId:(NSString*)instanceId {
    return [Lockbox archiveObject:instanceId forKey:kInstanceId];
}

+ (NSString*)getInstanceId {
    return [Lockbox unarchiveObjectForKey:kInstanceId];
}

+ (NSArray*)getSavedCardsInfo {
    return [Lockbox unarchiveObjectForKey:kCardsInfo];
}

+ (BOOL)saveUserCardInfo:(YMAMoneySourceModel*)cardInfo {
    NSMutableArray* muteArray = [NSMutableArray array];
    [muteArray addObjectsFromArray:[self getSavedCardsInfo]];
    
    if (muteArray && muteArray.count > 0) {
        [muteArray addObject:cardInfo];
        return [Lockbox archiveObject:muteArray forKey:kCardsInfo];
    }
    [muteArray addObject:cardInfo];
    return [Lockbox archiveObject:muteArray forKey:kCardsInfo];
}

@end
