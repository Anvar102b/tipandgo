//
//  ABError.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABError.h"

@implementation ABError

- (instancetype)initWithText:(NSString*)text exceptionType:(NSString*)type ticketId:(NSString*)ticketId
{
    self = [super init];
    if (self) {
        _text = text;
        _exceptionType = [self exceptionType:type];
        _ticketId = ticketId;
    }
    return self;
}

- (instancetype)initWithText:(NSString*)text {
    return [self initWithText:text exceptionType:0 ticketId:nil];
}

- (NSInteger)exceptionType:(NSString*)typeString {
    
    NSDictionary* types = @{@"None"         : @(0),
                            @"WrongCode"    : @(WrongCode)};
    return [types[typeString] integerValue];
}

@end
