//
//  ABWebViewViewController.h
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABWebViewViewControllerDelegate <NSObject>

- (void)paymentSuccess;

@end

@class YMAAscModel;

@interface ABWebViewViewController : UIViewController

@property (nonatomic, weak) id <ABWebViewViewControllerDelegate> delegate;

- (void)setAsc:(YMAAscModel*)asc;

@end
