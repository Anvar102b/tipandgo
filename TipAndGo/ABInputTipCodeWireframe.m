//
//  ABInputTipCodeWireframe.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABInputTipCodeWireframe.h"
#import "ABTermsConditionsViewController.h"
#import "ABPayProfileWireframe.h"
#import "ABProfile.h"

@implementation ABInputTipCodeWireframe

- (void)openProfile:(ABProfile*)profile from:(UIViewController*)fromViewController {
    [ABPayProfileWireframe presentABPayProfileModule:profile from:fromViewController];
}

- (void)openTermsConditions:(UIViewController*)fromViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ABTermsConditionsViewController *vc = (ABTermsConditionsViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ABTermsConditionsViewController"];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [fromViewController presentViewController:nav animated:YES completion:nil];
}

@end
