//
//  ABYandexMoneyBackend.h
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YMAAscModel;
@class YMAMoneySourceModel;

@interface ABYandexMoneyBackend : NSObject

+ (ABYandexMoneyBackend*) sharedManager;

- (NSArray*)savedCards;

/**
 InstanceId - получаем первый раз запросом, потом храним в keyChain
 */
- (void)checkForInstanceId:(void(^)())success
                   failure:(void(^)(ABError* error))failure;

- (void)paymentRequestWallet:(NSString*)wallet
                   tipAmount:(NSString*)tipAmmount
                     success:(void(^)())success
                     failure:(void(^)(ABError* error))failure;

- (void)processPaymentRequestAskMoneySource:(void(^)())success
                               authRequired:(void(^)(YMAAscModel *asc))authRequired
                                    failure:(void(^)(ABError* error))failure;

- (void)processPaymentRequestWithCardInfo:(YMAMoneySourceModel*)cardInfo
                                      csc:(NSString*)csc
                                  success:(void(^)())success
                             authRequired:(void(^)(YMAAscModel *asc))authRequired
                                  failure:(void(^)(ABError* error))failure;

@end
