//
//  UIAlertController+Window.h
//  KMModulbank
//
//  Created by Anvar Basharov on 05.05.16.
//  Copyright © 2016 ModulBank. All rights reserved.
//

#import <UIKit/UIKit.h>

//GlobalAlertController

@interface UIAlertController (Window)

- (void)show;
- (void)show:(BOOL)animated;

@end
