//
//  ABURLSession.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    GET,
    POST,
    PATCH,
    DELETE,
} RequestType;

@interface ABURLSession : NSObject

+ (NSString*)baseUrl;

+ (NSString*)urlString:(NSString*)method;

+ (NSURLSessionDataTask*)requestWithMethod:(RequestType)type urlString:(NSString*)urlString parameters:(NSDictionary*)parameters
                                   success:(void(^)(id response))success
                                   failure:(void(^)(ABError *error))failure;

@end
