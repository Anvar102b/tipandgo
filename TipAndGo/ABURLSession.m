//
//  ABURLSession.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABURLSession.h"
#import <UIKit/UIKit.h>

NSString* const baseURLString = @"http://api.tipngo.ru";

@implementation ABURLSession

+ (NSString*)baseUrl {
    return baseURLString;
}

+ (NSString*)urlString:(NSString*)method {
    return [NSString stringWithFormat:@"%@%@", baseURLString, method];
}

+ (NSURLSessionDataTask*)requestWithMethod:(RequestType)type urlString:(NSString*)urlString parameters:(NSDictionary*)parameters
                           success:(void(^)(id response))success
                           failure:(void(^)(ABError *error))failure {
    NetworkActivityIndicatorVisible;
    NSString* httpMethod;
    switch (type) {
        case GET: {
            httpMethod = @"GET";
            if (parameters) {
                urlString = [NSString stringWithFormat:@"%@%@", urlString, [self urlWithParameters:parameters]];
                urlString = [urlString substringToIndex:urlString.length - 1];
                urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
            break;
        }
        case POST: {
            httpMethod = @"POST";
            break;
        }
        case PATCH: {
            httpMethod = @"PATCH";
            break;
        }
        case DELETE: {
            httpMethod = @"DELETE";
            break;
        }
        default:
            break;
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:15.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"ios" forHTTPHeaderField:@"PLATFORM"];
    [request setHTTPMethod:httpMethod];
    if (type != GET) {
        if (parameters) {
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:&error];
            [request setHTTPBody:postData];
        }
    }
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    configuration.URLCache = nil;
    configuration.URLCredentialStorage = nil;
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    NSURLSessionDataTask *sessionTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NetworkActivityIndicatorNotVisible;
        [self data:data response:response error:error completionHandler:^(id response) {
            success(response);
        } failure:^(ABError *error) {
            failure(error);
        }];
    }];
    [sessionTask resume];
    return sessionTask;
}

#pragma mark - Private Methods

+ (void)data:(NSData*)data response:(NSURLResponse*)response error:(NSError*)error completionHandler:(void(^)(id response))success
     failure:(void(^)(ABError* error))failure {
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    switch (httpResponse.statusCode) {
        case 200: {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            NSLog(@"%@", json);
            dispatch_async(dispatch_get_main_queue(), ^{
                success(json);
            });
            break;
        }
        case 400:
        case 401:
        case 403:
        case 404:
        case 405:
        case 406:{
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
                NSLog(@"%@", json);
                ABError* error = [[ABError alloc] initWithText:json[@"defaultText"] exceptionType:json[@"exceptionType"] ticketId:@"ticketId"];
                failure(error);
            });
            break;
        }
        case 500: {
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            NSLog(@"%@", json);
            NSLog(@"Status Code: 500. Ошибка на сервере. Проверьте параметры и заголовок запроса.");
            break;
        }
        default:
            if (error) {
                switch (error.code) {
                    case -1001: {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString* text = @"Превышено допустимое время ожидания ответа от сервера. Возможно, соединение с интернетом прервано. Попробуйте повторить действие позже";
                            ABError* error = [[ABError alloc] initWithText:text];
                            failure(error);
                        });
                        break;
                    }
                    case -1009: {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString* text = @"Отсутствует подключение к сети";
                            ABError* error = [[ABError alloc] initWithText:text];
                            failure(error);
                        });
                        break;
                    }
                    default:
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSString* text = @"Неизвестная ошибка";
                            ABError* error = [[ABError alloc] initWithText:text];
                            failure(error);
                        });
                        break;
                }
            }
            break;
    }
}

+ (NSString*)urlWithParameters:(NSDictionary*)parameters {
    NSMutableString* urlString = [NSMutableString stringWithFormat:@"?"];
    NSArray* keys = parameters.allKeys;
    for (id object in keys) {
        if ([parameters[object] isKindOfClass:[NSArray class]]) {
            for (id obj in parameters[object])
                [urlString appendString:[NSString stringWithFormat:@"%@=%@&", object, obj]];
        } else {
            [urlString appendString:[NSString stringWithFormat:@"%@=%@&", object, parameters[object]]];
        }
    }
    return urlString;
}

@end
