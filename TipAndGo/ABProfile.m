//
//  ABProfile.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABProfile.h"
#import "ABURLSession.h"

@implementation ABProfile
/*
 {
 AvatarId = "<null>";
 Code = 91510;
 CompanyName = TestCompany;
 FirstName = Test1;
 JobTitle = "";
 LastName = Test11;
 YandexMoney = TestYandex;
 }
 @property (nonatomic, strong) NSURL* avatarUrl;
 @property (nonatomic, strong) NSNumber* code;
 @property (nonatomic, strong) NSString* companyName;
 @property (nonatomic, strong) NSString* firstName;
 @property (nonatomic, strong) NSString* lastName;
 @property (nonatomic, strong) NSString* jobTitle;
 @property (nonatomic, strong) NSString* yandexWallet;

 */
- (instancetype)initWithResponse:(NSDictionary*)response
{
    self = [super init];
    if (self) {
        [response[@"AvatarId"] isKindOfClass:[NSNull class]] ? (_avatarUrl = nil) : (_avatarUrl = [self makeAvatarurl:response[@"AvatarId"]]);
        [response[@"Code"] isKindOfClass:[NSNull class]] ? (_code = nil) : (_code = response[@"Code"]);
        [response[@"CompanyName"] isKindOfClass:[NSNull class]] ? (_companyName = nil)  : (_companyName = [NSString stringWithFormat:@"%@",response[@"CompanyName"]]);
        [response[@"FirstName"] isKindOfClass:[NSNull class]] ? (_firstName = nil) : (_firstName = response[@"FirstName"]);
        [response[@"JobTitle"] isKindOfClass:[NSNull class]] ? (_jobTitle = nil) : (_jobTitle = response[@"JobTitle"]);
        [response[@"LastName"] isKindOfClass:[NSNull class]] ? (_lastName = nil) : (_lastName = response[@"LastName"]);
        [response[@"YandexMoney"] isKindOfClass:[NSNull class]] ? (_yandexWallet = nil) : (_yandexWallet = [NSString stringWithFormat:@"%@", response[@"YandexMoney"]]);
//        if (_code && ![_code isEqual:@""]) {
//            _avatarUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@/client/profile/%@/avatar",[ABURLSession baseUrl], _code]];
//        }
    }
    return self;
}
- (NSURL*)makeAvatarurl:(NSString*)avatarId {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/client/profile/avatar/%@", [ABURLSession baseUrl],avatarId]];
    
    
}

@end
