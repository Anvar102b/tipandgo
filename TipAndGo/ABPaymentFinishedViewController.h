//
//  ABPaymentFinishedViewController.h
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ABProfile;

@interface ABPaymentFinishedViewController : UIViewController

- (void)setProfile:(ABProfile*)profile;

@end
