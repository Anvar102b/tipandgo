//
//  ABCommonHelper.h
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YMAMoneySourceModel.h"

typedef enum
{
    ScreenSize_3_5_inch,
    ScreenSize_4_inch,
    ScreenSize_4_7_inch,
    ScreenSize_5_5_inch,
    
} ScreenSizeInch;

@interface ABCommonHelper : NSObject

+(ScreenSizeInch)screenSizeInch;

+ (NSString*)cardType:(YMAPaymentCardType)cardType;
+ (NSString*)cardNums:(NSString*)cardNumber;

@end
