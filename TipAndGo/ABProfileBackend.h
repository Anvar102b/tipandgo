//
//  ABRecipientBackend.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABProfile;

@interface ABProfileBackend : NSObject

+ (NSURLSessionDataTask*)loadProfile:(NSString*)tipCode
                             success:(void(^)(ABProfile* profile))success
                             failure:(void(^)(ABError* error))failure;

+ (NSURLSessionDataTask*)addPayment:(NSString*)amount
                            tipCode:(NSString*)tipCode
                            success:(void(^)())success
                            failure:(void(^)(ABError* error))failure;

@end
