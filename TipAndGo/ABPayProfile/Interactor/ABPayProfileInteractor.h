//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABPayProfileInteractorInput.h"

@protocol ABPayProfileInteractorOutput;

@interface ABPayProfileInteractor : NSObject <ABPayProfileInteractorInput>

@property (nonatomic, weak) id <ABPayProfileInteractorOutput> presenter;

@end
