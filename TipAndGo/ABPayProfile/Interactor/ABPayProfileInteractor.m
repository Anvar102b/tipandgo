//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import "ABPayProfileInteractor.h"
#import "ABPayProfilePresenter.h"

#import "ABYandexMoneyBackend.h"
#import "ABProfileBackend.h"
#import "Lockbox.h"

#import "ABProfile.h"
#import "YMAMoneySourceModel.h"
#import "YMAAscModel.h"

@interface ABPayProfileInteractor ()

@property (nonatomic, strong) NSString* tipValue;
@property (nonatomic, strong) NSArray* tipValuesArray;
@property (nonatomic, strong) ABProfile* profile;

@end

@implementation ABPayProfileInteractor

- (instancetype)init
{
    self = [super init];
    if (self) {
        _tipValuesArray = @[@{@"Amount" : @"50", @"Tapped" : @(NO)},
                            @{@"Amount" : @"100", @"Tapped" : @(NO)},
                            @{@"Amount" : @"500", @"Tapped" : @(NO)},
                            @{@"Amount" : @"Другое", @"Tapped" : @(NO)},
                            ];
    }
    return self;
}

#pragma mark - ABPayProfileInteractorInput

- (void)setProfile:(ABProfile *)profile {
    _profile = profile;
}

- (NSArray*)tipValuesArray {
    return _tipValuesArray;
}

- (void)didTipValueTapped:(NSInteger)tipValueTag {
    _tipValuesArray = [self uncheckAllTips];
    switch (tipValueTag) {
        case 1:
            _tipValue = _tipValuesArray[0][@"Amount"];
            _tipValuesArray = [self checkTipValue:0];
            [_presenter enablePay];
            break;
        case 2:
            _tipValue = _tipValuesArray[1][@"Amount"];
            _tipValuesArray = [self checkTipValue:1];
            [_presenter enablePay];
            break;
        case 3:
            _tipValue = _tipValuesArray[2][@"Amount"];
            _tipValuesArray = [self checkTipValue:2];
            [_presenter enablePay];
            break;
        case 4:
            _tipValue = _tipValuesArray[3][@"Amount"];
            _tipValuesArray = [self checkTipValue:3];
            
            if ([_tipValue isEqual:@"Другое"]) {
                [_presenter disablePay];
            } else {
                [_presenter enablePay];
            }
            [_presenter openOtherTipValueView];
        default:
            break;
    }
}

- (void)payPressed {
    [[ABYandexMoneyBackend sharedManager] checkForInstanceId:^{
        NSArray* savedCards = [[ABYandexMoneyBackend sharedManager] savedCards];
        if (savedCards && savedCards.count > 0 ) {
            [_presenter haveSavedCards:savedCards];
        } else {
            [self makePaymentRequestWithNoCardInfo];
        }
    } failure:^(ABError *error) {
        [_presenter didFailedLoading:error];
    }];
}

- (void)makePaymentWithCardInfo:(YMAMoneySourceModel*)cardInfo csc:(NSString*)csc {
    [[ABYandexMoneyBackend sharedManager] paymentRequestWallet:_profile.yandexWallet tipAmount:_tipValue success:^{
        [[ABYandexMoneyBackend sharedManager] processPaymentRequestWithCardInfo:cardInfo csc:csc success:^{
            [_presenter paymentFinished];
        } authRequired:^(YMAAscModel *asc) {
            [_presenter openWebViewControllerWith:asc];
        } failure:^(ABError *error) {
            [_presenter didFailedLoading:error];
        }];
    } failure:^(ABError *error) {
        [_presenter didFailedLoading:error];
    }];
}

- (void)otherCardPayment {
    [self makePaymentRequestWithNoCardInfo];
}

- (void)paymentFinished {
    [ABProfileBackend addPayment:_tipValue tipCode:_profile.code success:^{
        
    } failure:^(ABError *error) {
        
    }];
}

- (void)didEnteredTipValue:(NSString*)tipValue {
    NSMutableArray* muteArray = [NSMutableArray arrayWithArray:_tipValuesArray];
    NSMutableDictionary* dict =  [NSMutableDictionary dictionaryWithDictionary:muteArray[3]];
    [dict setObject:tipValue forKey:@"Amount"];
    [muteArray replaceObjectAtIndex:3 withObject:dict];
    _tipValuesArray = muteArray;
    _tipValuesArray = [self checkTipValue:3];
    _tipValue = _tipValuesArray[3][@"Amount"];
    [_presenter didOtherTipValueChanged];
    [_presenter enablePay];
}

#pragma mark - Private methods

- (void)makePaymentRequestWithNoCardInfo {
    [[ABYandexMoneyBackend sharedManager] paymentRequestWallet:_profile.yandexWallet tipAmount:_tipValue success:^{
        [[ABYandexMoneyBackend sharedManager] processPaymentRequestAskMoneySource:^{
            
        } authRequired:^(YMAAscModel *asc) {
            [_presenter openWebViewControllerWith:asc];
        } failure:^(ABError *error) {
            [_presenter didFailedLoading:error];
        }];
    } failure:^(ABError *error) {
        [_presenter didFailedLoading:error];
    }];
}

- (NSArray*)uncheckAllTips {
    NSMutableArray* muteArray = [NSMutableArray array];
    for (NSDictionary* dict in _tipValuesArray) {
        NSMutableDictionary* muteDict = [NSMutableDictionary dictionaryWithDictionary:dict];
        [muteDict setObject:@(NO) forKey:@"Tapped"];
        [muteArray addObject:muteDict];
    }
    return muteArray;
}

- (NSArray*)checkTipValue:(NSInteger)index {
    NSMutableArray* muteArray = [NSMutableArray arrayWithArray:_tipValuesArray];
    NSMutableDictionary* muteDict = [NSMutableDictionary dictionaryWithDictionary:muteArray[index]];
    [muteDict setObject:@(YES) forKey:@"Tapped"];
    [muteArray replaceObjectAtIndex:index withObject:muteDict];
    return muteArray;
}

@end
