//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import "ABPayProfileWireframe.h"
#import "ABPayProfileViewController.h"
#import "ABPayProfilePresenter.h"
#import "ABPayProfileInteractor.h"
#import "ABPayProfileWireframe.h"
#import "ABProfile.h"
#import "YMAAscModel.h"
#import "ABWebViewViewController.h"
#import "ABPaymentFinishedViewController.h"
#import "ABOtherTipValueViewController.h"

@implementation ABPayProfileWireframe

+ (void)presentABPayProfileModule:(ABProfile*)profile from:(UIViewController*)fromViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ABPayProfileViewController *view = (ABPayProfileViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"ABPayProfileViewController"];
    ABPayProfilePresenter *presenter = [ABPayProfilePresenter new];
    ABPayProfileInteractor *interactor = [ABPayProfileInteractor new];
    ABPayProfileWireframe *wireframe= [ABPayProfileWireframe new];
    
    view.presenter = presenter;
    presenter.view = view;
    presenter.wireframe = wireframe;
    presenter.interactor = interactor;
    interactor.presenter = presenter;
    [presenter setProfileObject:profile];
    [fromViewController.navigationController pushViewController:view animated:YES];
}

- (void)openWebViewControllerWith:(YMAAscModel*)asc from:(UIViewController<ABWebViewViewControllerDelegate>*)fromViewController {
    ABWebViewViewController* vc = [ABWebViewViewController new];
    vc.delegate = fromViewController;
    [vc setAsc:asc];
    UINavigationController* nav = [[UINavigationController alloc] initWithRootViewController:vc];
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [fromViewController presentViewController:nav animated:YES completion:nil];
}

- (void)openPaymentFinished:(ABProfile*)profile from:(UIViewController*)fromViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ABPaymentFinishedViewController* vc = (ABPaymentFinishedViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"ABPaymentFinishedViewController"];
    [vc setProfile:profile];
    [fromViewController.navigationController pushViewController:vc animated:YES];
    
}

- (void)openOtherTipValueViewfrom:(UIViewController<ABOtherTipValueViewControllerDelegate>*)fromViewController {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    ABOtherTipValueViewController* vc = (ABOtherTipValueViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"ABOtherTipValueViewController"];
    vc.delegate = fromViewController;
    vc.modalPresentationStyle = UIModalPresentationOverFullScreen;
    vc.view.backgroundColor = [UIColor clearColor];
    [fromViewController presentViewController:vc animated:YES completion:nil];
}

@end


