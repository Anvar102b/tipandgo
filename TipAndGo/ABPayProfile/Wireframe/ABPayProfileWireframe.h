//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABPayProfileWireframeInput.h"
#import <UIKit/UIKit.h>

@class ABProfile;

@interface ABPayProfileWireframe : NSObject <ABPayProfileWireframeInput>

+ (void)presentABPayProfileModule:(ABProfile*)profile from:(UIViewController*)fromViewController;

@end
