//
//  ABTipValuesTableViewCell.h
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ABTipValuesTableViewCellDelegate <NSObject>

- (void)didTipValueTapped:(NSInteger)valueTag;

@end

@interface ABTipValuesTableViewCell : UITableViewCell

@property (nonatomic, weak) id <ABTipValuesTableViewCellDelegate> delegate;

- (void)setTipValues:(NSArray*)tipValues;
+ (CGFloat)heightForRow;

@end
