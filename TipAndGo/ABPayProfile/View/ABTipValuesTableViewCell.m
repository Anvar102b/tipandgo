//
//  ABTipValuesTableViewCell.m
//  TipAndGo
//
//  Created by Anvar Basharov on 09.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABTipValuesTableViewCell.h"

@interface ABTipValuesTableViewCell ()

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *tipButtons;

@end

@implementation ABTipValuesTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

+ (CGFloat)heightForRow {
    CGFloat height;
    switch ([ABCommonHelper screenSizeInch]) {
        case ScreenSize_3_5_inch:
        case ScreenSize_4_inch:
            height = 95.f;
            break;
        default:
            height = 114.f;
            break;
    }
    return height;
}

- (IBAction)tipValueTapped:(UIButton*)sender {
    [self configureButtons];
    sender.backgroundColor = BlueColor;
    [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_delegate didTipValueTapped:sender.tag];
}

#pragma mark - Public Methods

- (void)configureButtons {
    for (UIButton* button in _tipButtons) {
        button.layer.cornerRadius = [self cornerRadius];
        button.layer.masksToBounds = YES;
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
}

- (void)setTipValues:(NSArray*)tipValues {
    for (NSInteger i = 0; i < _tipButtons.count; i++) {
        UIButton* button = _tipButtons[i];
        NSString* valueString = tipValues[i][@"Amount"];
        if ([valueString isEqual:@"Другое"] == NO)
            valueString = [NSString stringWithFormat:@"%@%@", valueString, RUB];
        
        NSValue* checked = tipValues[i][@"Tapped"];
        if ([checked  isEqual: @(YES)]) {
            [self configureChekedButton:button];
        } else{
            [self configureUnchekedButton:button];
        }
        [button setTitle:valueString forState:UIControlStateNormal];
    }
}

- (void)configureUnchekedButton:(UIButton*)button {
        button.layer.cornerRadius = [self cornerRadius];
        button.layer.masksToBounds = YES;
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = [UIColor blackColor].CGColor;
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (void)configureChekedButton:(UIButton*)button {
    button.layer.cornerRadius = [self cornerRadius];
    button.layer.masksToBounds = YES;
    button.layer.borderWidth = 1.0f;
    button.layer.borderColor = [UIColor blackColor].CGColor;
    button.backgroundColor = BlueColor;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

- (NSInteger)cornerRadius {
    NSInteger cornerRadius;
    switch ([ABCommonHelper screenSizeInch]) {
        case ScreenSize_3_5_inch:
        case ScreenSize_4_inch:
            cornerRadius = 47.f/2.f;
            break;
        default:
            cornerRadius = 55.f/2.f;
            break;
    }
    return cornerRadius;
}

@end
