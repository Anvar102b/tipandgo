//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABWebViewViewController.h"
#import "ABPayProfileViewInput.h"
#import "ABOtherTipValueViewController.h"

@protocol ABPayProfileViewOutput;

@interface ABPayProfileViewController : UIViewController <ABPayProfileViewInput, ABWebViewViewControllerDelegate, ABOtherTipValueViewControllerDelegate>

@property (nonatomic, strong) id <ABPayProfileViewOutput> presenter;

@end
