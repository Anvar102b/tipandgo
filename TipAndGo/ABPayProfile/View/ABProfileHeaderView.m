//
//  ABProfileHeaderView.m
//  TipAndGo
//
//  Created by Anvar Basharov on 08.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABProfileHeaderView.h"

#import <SDWebImage/UIImageView+WebCache.h>

#import "ABProfile.h"

@interface ABProfileHeaderView ()

@property (nonatomic, weak) IBOutlet UIImageView* profilePhoto;
@property (nonatomic, weak) IBOutlet UILabel* nameDetails;
@property (nonatomic, weak) IBOutlet UILabel* jobDetails;

@end

@implementation ABProfileHeaderView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        UINib *nib;
        if (self.subviews.count == 0) {
            if ([[UIScreen mainScreen] bounds].size.width == 320) {
                nib = [UINib nibWithNibName:@"ABProfileHeaderViewCompact" bundle:nil];
            } else {
                nib = [UINib nibWithNibName:@"ABProfileHeaderView" bundle:nil];
            }
            UIView *subview = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            subview.frame = self.bounds;
            [self addSubview:subview];
        }
    }
    return self;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    self.profilePhoto.layer.cornerRadius = CGRectGetHeight(self.profilePhoto.bounds) / 2.f;
    self.profilePhoto.layer.masksToBounds = YES;
    self.profilePhoto.layer.borderWidth = 2.f;
    self.profilePhoto.layer.borderColor = BlueColor.CGColor;
}

- (void)setProfile:(ABProfile*)profile {
    if (profile.avatarUrl) {
        [_profilePhoto sd_setImageWithURL:profile.avatarUrl
                     placeholderImage:[UIImage imageNamed:@"Avatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                         profile.avatarImage = image;
                     }];
    } else {
        _profilePhoto.image = [UIImage imageNamed:@"Avatar"];
    }
    _nameDetails.text = [NSString stringWithFormat:@"%@ %@", profile.firstName, profile.lastName];
    _jobDetails.text =[NSString stringWithFormat:@"%@ | %@", profile.jobTitle, profile.companyName];
}

@end
