//
//  ABProfileHeaderView.h
//  TipAndGo
//
//  Created by Anvar Basharov on 08.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABProfile;

@interface ABProfileHeaderView : UIView

- (void)setProfile:(ABProfile*)profile;

@end
