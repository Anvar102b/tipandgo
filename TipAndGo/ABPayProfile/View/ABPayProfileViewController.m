//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import "ABPayProfileViewController.h"
#import "ABPayProfilePresenter.h"
#import "ABProfileHeaderView.h"
#import "ABTipValuesTableViewCell.h"
#import "ABWebViewViewController.h"

#import "ABProgressHUD.h"

#import "ABProfile.h"
#import "YMAMoneySourceModel.h"

@interface ABPayProfileViewController () <UITableViewDelegate, UITableViewDataSource, ABTipValuesTableViewCellDelegate>

@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, weak) IBOutlet UIButton* payButton;

@end

@implementation ABPayProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initObjects];
}

#pragma mark - InitObjects

- (void)initObjects {
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0.f, 0.f, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds)) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableFooterView = [UIView new];
    _tableView.alwaysBounceVertical = NO;
    ABProfileHeaderView *view = [[[NSBundle mainBundle] loadNibNamed:[self headerView] owner:self options:nil]firstObject];
    [view setProfile:[_presenter getProfile]];
    [self.tableView registerNib:[UINib nibWithNibName:[self tipValuseCell] bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ABTipValuesTableViewCell"];
    _tableView.tableHeaderView = view;
    [self.view addSubview:_tableView];
    
    _payButton.layer.cornerRadius = CGRectGetHeight(_payButton.frame) / 2.f;
    _payButton.layer.masksToBounds = YES;
    _payButton.enabled = NO;
    [self.view bringSubviewToFront:_payButton];
}

#pragma mark - ABPayProfileViewInput

- (void)showLoadingAnimation {
    [ABProgressHUD show];
}

- (void)hideLoadingAnimation {
    [ABProgressHUD hide];
}

- (void)enablePayButton {
    _payButton.enabled = YES;
}

- (void)disablePayButton {
    _payButton.enabled = NO;
}

- (void)reloadTable {
    [_tableView reloadData];
}

- (void)showErrorMessage:(ABError*)error {
    if ([ABProgressHUD isVisable]) {
        [self hideLoadingAnimation];
    }
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:error.text preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* okbtn = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okbtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)showActionSheetWithCards:(NSArray*)cards {
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Выберите карту для оплаты" message:nil preferredStyle:UIAlertControllerStyleActionSheet];

    for (YMAMoneySourceModel* cardInfo in cards) {
        NSString* cardName = [NSString stringWithFormat:@"%@ %@", [ABCommonHelper cardType:cardInfo.cardType], [ABCommonHelper cardNums:cardInfo.panFragment]];
        UIAlertAction* cardButton = [UIAlertAction actionWithTitle:cardName style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self showCscAlert:cardInfo];
        }];
        [alert addAction:cardButton];
    }
    UIAlertAction* otherCard = [UIAlertAction actionWithTitle:@"С другой карты" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [_presenter otherCardPayment];
    }];
    [alert addAction:otherCard];
    
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancel];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"ABTipValuesTableViewCell";
    
    ABTipValuesTableViewCell *cell = (ABTipValuesTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ABTipValuesTableViewCell" owner:self options:nil];
        cell = (ABTipValuesTableViewCell *)[nib objectAtIndex:0];
    }
    [cell setTipValues:[_presenter tipValues]];
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height;
    switch (indexPath.row) {
        case 0: {
            height = [ABTipValuesTableViewCell heightForRow];
        }
            break;
            
        default:
            break;
    }
    return height;
}

#pragma mark - ABTipValuesTableViewCellDelegate

- (void)didTipValueTapped:(NSInteger)valueTag {
    [_presenter didTipValueTapped:valueTag];
}

#pragma mark - ABWebViewViewControllerDelegate

- (void)paymentSuccess {
    [_presenter openPaymentFinished:[_presenter getProfile] from:self];
}

#pragma mark - ABOtherTipValueViewControllerDelegate

- (void)didEnteredTipValue:(NSString*)tipValue {
    [_presenter didEnteredTipValue:tipValue];
}

#pragma mark - Actions

- (IBAction)payTipAction:(id)sender {
    [_presenter payPressed];
}

#pragma mark - Public Methods


#pragma mark - Private Methods

- (NSString*)headerView {
    NSString* headerView;
    switch ([ABCommonHelper screenSizeInch]) {
        case ScreenSize_3_5_inch:
        case ScreenSize_4_inch:
            headerView = @"ABProfileHeaderViewCompact";
            break;
        default:
            headerView = @"ABProfileHeaderView";
            break;
    }
    return headerView;
}

- (NSString*)tipValuseCell {
    NSString* tipValuesCell;
    switch ([ABCommonHelper screenSizeInch]) {
        case ScreenSize_3_5_inch:
        case ScreenSize_4_inch:
            tipValuesCell = @"ABTipValuesTableViewCellCompact";
            break;
        default:
            tipValuesCell = @"ABTipValuesTableViewCell";
            break;
    }
    return tipValuesCell;
}

- (void)showCscAlert:(YMAMoneySourceModel*)cardInfo {
    NSString* cardName = [NSString stringWithFormat:@"%@ %@", [ABCommonHelper cardType:cardInfo.cardType], [ABCommonHelper cardNums:cardInfo.panFragment]];
    NSString* title = [NSString stringWithFormat:@"CVV код карты %@", cardName];
    UIAlertController *alerController = [UIAlertController alertControllerWithTitle:title message:@"Код находится с обратной стороны карты" preferredStyle:UIAlertControllerStyleAlert];
    [alerController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"CVV";
        textField.secureTextEntry = YES;
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"Current csc %@", [[alerController textFields][0] text]);
        NSString* csc = [[alerController textFields][0] text];
        [_presenter makePaymentWithCardInfo:cardInfo csc:csc];
        
    }];
    [alerController addAction:confirmAction];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Отмена" style:UIAlertActionStyleCancel handler:nil];
    [alerController addAction:cancelAction];
    [self presentViewController:alerController animated:YES completion:nil];
}

@end
