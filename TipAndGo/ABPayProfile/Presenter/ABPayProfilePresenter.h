//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABPayProfileViewOutput.h"
#import "ABPayProfileInteractorOutput.h"

@protocol ABPayProfileViewInput;
@protocol ABPayProfileInteractorInput;
@protocol ABPayProfileWireframeInput;

@class ABProfile;

@interface ABPayProfilePresenter : NSObject <ABPayProfileViewOutput, ABPayProfileInteractorOutput>

@property (nonatomic, weak) id <ABPayProfileViewInput> view;
@property (nonatomic, strong) id <ABPayProfileInteractorInput> interactor;
@property (nonatomic, strong) id <ABPayProfileWireframeInput> wireframe;

- (void)setProfileObject:(ABProfile*)profile;

@end
