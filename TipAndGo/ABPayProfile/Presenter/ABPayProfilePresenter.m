//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import "ABPayProfilePresenter.h"
#import "ABPayProfileWireframe.h"
#import "ABPayProfileInteractor.h"
#import "ABPayProfileViewController.h"

#import "ABProfile.h"
#import "YMAMoneySourceModel.h"
#import "YMAAscModel.h"

@interface ABPayProfilePresenter ()

@property (nonatomic, strong) ABProfile* profile;

@end

@implementation ABPayProfilePresenter

#pragma mark - ABPayProfileViewOutput

- (ABProfile*)getProfile {
    return _profile;
}

- (NSArray*)tipValues {
    return [_interactor tipValuesArray];
}

- (void)didTipValueTapped:(NSInteger)tipValueTag {
    [_interactor didTipValueTapped:tipValueTag];
}

- (void)payPressed {
    [_view showLoadingAnimation];
    [_interactor payPressed];
}

- (void)makePaymentWithCardInfo:(YMAMoneySourceModel*)cardInfo csc:(NSString*)csc {
    [_view showLoadingAnimation];
    [_interactor makePaymentWithCardInfo:cardInfo csc:csc];
}

- (void)otherCardPayment {
    [_view showLoadingAnimation];
    [_interactor otherCardPayment];
}

- (void)openPaymentFinished:(ABProfile*)profile from:(UIViewController<ABWebViewViewControllerDelegate>*)fromViewController {
    [_interactor paymentFinished];
    [_wireframe openPaymentFinished:profile from:fromViewController];
}

- (void)didEnteredTipValue:(NSString*)tipValue {
    [_interactor didEnteredTipValue:tipValue];
}

#pragma mark - ABPayProfileInteractorOutput

- (void)didFailedLoading:(ABError*)error {
    [_view showErrorMessage:error];
}

- (void)haveSavedCards:(NSArray*)cards {
    [_view hideLoadingAnimation];
    [_view showActionSheetWithCards:cards];
}

- (void)openWebViewControllerWith:(YMAAscModel*)asc {
    [_view hideLoadingAnimation];
    [_wireframe openWebViewControllerWith:asc from:(ABPayProfileViewController*)_view];
    
}

- (void)paymentFinished {
    [_view hideLoadingAnimation];
    [_wireframe openPaymentFinished:_profile from:(ABPayProfileViewController*)_view];
}

- (void)openOtherTipValueView {
    [_wireframe openOtherTipValueViewfrom:(ABPayProfileViewController*)_view];
}

- (void)enablePay {
    [_view enablePayButton];
}

- (void)disablePay {
    [_view disablePayButton];
}

- (void)didOtherTipValueChanged {
    [_view reloadTable];
}

#pragma mark - Public Methods

- (void)setProfileObject:(ABProfile*)profile {
    _profile = profile;
    [_interactor setProfile:profile];
}

@end
