//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ABProfile;
@class YMAMoneySourceModel;

@protocol ABPayProfileViewOutput <NSObject>

- (ABProfile*)getProfile;
- (NSArray*)tipValues;
- (void)didTipValueTapped:(NSInteger)tipValueTag;
- (void)payPressed;
- (void)makePaymentWithCardInfo:(YMAMoneySourceModel*)cardInfo csc:(NSString*)csc;
- (void)otherCardPayment;
- (void)openPaymentFinished:(ABProfile*)profile from:(UIViewController*)fromViewController;
- (void)didEnteredTipValue:(NSString*)tipValue;

@end
