//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class YMAAscModel;

@protocol ABPayProfileInteractorOutput <NSObject>

- (void)didFailedLoading:(ABError*)error;
- (void)haveSavedCards:(NSArray*)cards;
- (void)openWebViewControllerWith:(YMAAscModel*)asc;
- (void)paymentFinished;
- (void)openOtherTipValueView;
- (void)enablePay;
- (void)disablePay;
- (void)didOtherTipValueChanged;

@end
