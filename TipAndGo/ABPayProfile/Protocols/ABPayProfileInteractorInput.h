//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ABProfile;
@class YMAMoneySourceModel;

@protocol ABPayProfileInteractorInput <NSObject>

- (NSArray*)tipValuesArray;
- (void)didTipValueTapped:(NSInteger)tipValueTag;
- (void)payPressed;
- (void)setProfile:(ABProfile *)profile;
- (void)makePaymentWithCardInfo:(YMAMoneySourceModel*)cardInfo csc:(NSString*)csc;
- (void)otherCardPayment;
- (void)paymentFinished;
- (void)didEnteredTipValue:(NSString*)tipValue;

@end
