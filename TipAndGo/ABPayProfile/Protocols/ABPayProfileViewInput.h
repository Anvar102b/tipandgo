//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABPayProfileViewInput <NSObject>

- (void)showErrorMessage:(ABError*)error;
- (void)showLoadingAnimation;
- (void)hideLoadingAnimation;
- (void)showActionSheetWithCards:(NSArray*)cards;
- (void)enablePayButton;
- (void)disablePayButton;
- (void)reloadTable;

@end
