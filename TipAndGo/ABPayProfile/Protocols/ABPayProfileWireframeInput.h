//
// Created by Anvar Basharov
// Copyright (c) 2016 Anvar Basharov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class YMAAscModel;
@class ABProfile;

@protocol ABWebViewViewControllerDelegate;
@protocol ABOtherTipValueViewControllerDelegate;

@protocol ABPayProfileWireframeInput <NSObject>

- (void)openWebViewControllerWith:(YMAAscModel*)asc from:(UIViewController*)fromViewController;
- (void)openPaymentFinished:(ABProfile*)profile from:(UIViewController<ABWebViewViewControllerDelegate>*)fromViewController;
- (void)openOtherTipValueViewfrom:(UIViewController<ABOtherTipValueViewControllerDelegate>*)fromViewController;

@end
