//
//  ABOtherTipValueViewController.m
//  TipAndGo
//
//  Created by Anvar Basharov on 14.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABOtherTipValueViewController.h"
#import "ABKeyboardView.h"

@interface ABOtherTipValueViewController () <ABKeyboardViewDelegate, UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIView* bgView;
@property (nonatomic, weak) IBOutlet UITextField* textField;
@property (nonatomic, weak) IBOutlet UILabel* minTiplabel;
@property (nonatomic, weak) IBOutlet ABKeyboardView* keyboardView;

@property (nonatomic, strong) NSMutableString* tipValueString;

@end

@implementation ABOtherTipValueViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initObjects];
}

#pragma mark - Init objects

- (void)initObjects {
    _tipValueString = [[NSMutableString alloc] initWithString:RUB];
    _keyboardView.delegate = self;
    _keyboardView.showDoneButton = NO;
    _textField.delegate = self;
    _textField.text = _tipValueString;
    _bgView.center = CGPointMake(CGRectGetMidX(self.view.frame), CGRectGetMidY(self.view.frame) - CGRectGetHeight(_bgView.bounds));
    _minTiplabel.text = [NSString stringWithFormat:@"Минимальная сумма %ld%@", (long)MIN_TIP, RUB];
}

#pragma mark - ABKeyboardViewDelegate

- (void)didpressNum:(NSInteger)number {
    if (_tipValueString.length == 5)
        return;
    [_tipValueString insertString:[NSString stringWithFormat:@"%ld", (long)number] atIndex:_tipValueString.length - 1];
    
    
    if ([self isGoodTip] == YES)
        [_keyboardView showDone];
    
    if (_tipValueString.length == 2)
        [_keyboardView enableDelete];
    _textField.text = _tipValueString;
}

- (void)didpressDelete {
    if (_tipValueString.length == 1)
        return;
    [_tipValueString deleteCharactersInRange:NSMakeRange(_tipValueString.length-2, 1)];
    
    if ([self isGoodTip] == NO)
        [_keyboardView hideDone];
    
    if (_tipValueString.length == 1)
        [_keyboardView disableDelete];
    _textField.text = _tipValueString;
}

- (void)didPressDone {
    [_delegate didEnteredTipValue:[self valueString]];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return NO;
}

#pragma mark - Actions

- (IBAction)close:(UIButton*)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Private Methods 

- (BOOL)isGoodTip {
    NSString* valueString = [self valueString];
    NSInteger tipValue = valueString.integerValue;
    return tipValue >= MIN_TIP ? YES : NO;
}

- (NSString*)valueString{
    return [_tipValueString substringToIndex:_tipValueString.length - 1];
}

@end
