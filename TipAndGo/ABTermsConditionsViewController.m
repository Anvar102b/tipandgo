//
//  ABTermsConditionsViewController.m
//  TipAndGo
//
//  Created by Anvar Basharov on 16.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABTermsConditionsViewController.h"
#import "ABProgressHUD.h"

@interface ABTermsConditionsViewController () <UIWebViewDelegate>

@property (nonatomic, weak) IBOutlet UIWebView* webView;

@end

@implementation ABTermsConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Готово" style:UIBarButtonItemStylePlain target:self  action:@selector(cancel:)];
    _webView.delegate = self;
    _webView.opaque = NO;
   // _webView.scalesPageToFit = YES;
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.tipngo.ru/privacy.html"]];
    [_webView loadRequest:request];
}


#pragma mark - UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ABProgressHUD hide];
    NetworkActivityIndicatorNotVisible;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ", (int)self.view.bounds.size.width]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ABProgressHUD show];
    NetworkActivityIndicatorVisible;
}

- (void)cancel:(id)sender {
    [_webView stopLoading];
    NetworkActivityIndicatorNotVisible;
    if ([ABProgressHUD isVisable]) {
        [ABProgressHUD hide];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
