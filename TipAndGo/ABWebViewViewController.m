//
//  ABWebViewViewController.m
//  TipAndGo
//
//  Created by Anvar Basharov on 11.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABWebViewViewController.h"
#import "YMAAscModel.h"
#import "ABProgressHUD.h"
#import "ABYandexMoneyBackend.h"

static NSString *const kHttpsScheme = @"https";
static NSString *const kSuccessUrl = @"yandexmoneyapp:oauth/authorize/success";
static NSString *const kFailUrl = @"yandexmoneyapp:oauth/authorize/fail";

@interface ABWebViewViewController () <UIWebViewDelegate>

@property (nonatomic, strong) YMAAscModel* asc;
@property (nonatomic,strong) UIWebView* webView;

@end

@implementation ABWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initObjects];
}

- (void)initObjects {
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Отмена" style:UIBarButtonItemStylePlain target:self  action:@selector(cancel:)];
    _webView = [[UIWebView alloc] initWithFrame:self.view.frame];
    _webView.delegate = self;
    _webView.opaque = NO;
    _webView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.backgroundColor = [UIColor whiteColor];
    _webView.scrollView.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    [self.view addSubview:_webView];
    [self processPaymentRequestWithAsc:_asc];
}

#pragma mark - Public Methods

- (void)setAsc:(YMAAscModel*)asc {
    _asc = asc;
}
#pragma mark - Private Methods

- (void)finishPayment {
    [[ABYandexMoneyBackend sharedManager] processPaymentRequestAskMoneySource:^{
        NetworkActivityIndicatorNotVisible;
        if ([ABProgressHUD isVisable]) {
            [ABProgressHUD hide];
        }
        [self dismissViewControllerAnimated:YES completion:^{
            [_delegate paymentSuccess];
        }];
    } authRequired:^(YMAAscModel *asc) {
        NetworkActivityIndicatorNotVisible;
        [self processPaymentRequestWithAsc:asc];
    } failure:^(ABError *error) {
        NetworkActivityIndicatorNotVisible;
        if ([ABProgressHUD isVisable]) {
            [ABProgressHUD hide];
        }
        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:error.text preferredStyle: UIAlertControllerStyleAlert];
        UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:okBtn];
        [self presentViewController:alert animated:YES completion:nil];        
    }];
}

- (void)showError:(ABError *)error {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:error.text preferredStyle: UIAlertControllerStyleAlert];
    UIAlertAction* okBtn = [UIAlertAction actionWithTitle:@"Ок" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)processPaymentRequestWithAsc:(YMAAscModel *)asc {
        // Process info about redirect to authorization page.
        NSMutableString *post = [NSMutableString string];
        for (NSString *key in asc.params.allKeys) {
            NSString *paramValue = [self addPercentEscapesToString:(asc.params)[key]];
            NSString *paramKey = [self addPercentEscapesToString:key];
            [post appendString:[NSString stringWithFormat:@"%@=%@&", paramKey, paramValue]];
        }
        
        if (post.length)
            [post deleteCharactersInRange:NSMakeRange(post.length - 1, 1)];
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:asc.url];
        NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO];
        [request setHTTPMethod:@"POST"];
        [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long) postData.length] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody:postData];
        [_webView loadRequest:request];
}

- (NSString *)addPercentEscapesToString:(NSString *)string {
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                  (__bridge CFStringRef)string,
                                                                                  NULL,
                                                                                  (CFStringRef)@";/?:@&=+$,",
                                                                                  kCFStringEncodingUTF8));
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if ([request URL] == nil) {
        [self.navigationController popToRootViewControllerAnimated:NO];
        return NO;
    }
    
    NSString *strippedURL        = [self strippedURL:request.URL];
    NSString *strippedSuccessURL = [self strippedURL:[NSURL URLWithString:kSuccessUrl]];
    NSString *strippedFailURL    = [self strippedURL:[NSURL URLWithString:kFailUrl]];
    
    if ([strippedURL isEqualToString:strippedSuccessURL]) {
        
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 2*NSEC_PER_SEC);
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
            [self finishPayment];
        });
        
        [webView removeFromSuperview];
        
        return NO;
    }
    
    if ([strippedURL isEqualToString:strippedFailURL]) {
        [self showError:nil];
        [webView removeFromSuperview];
        return NO;
    }
    
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [ABProgressHUD hide];
    NetworkActivityIndicatorNotVisible;
    [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"document.querySelector('meta[name=viewport]').setAttribute('content', 'width=%d;', false); ", (int)self.webView.bounds.size.width]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [ABProgressHUD show];
    NetworkActivityIndicatorVisible;
}

- (NSString *)strippedURL:(NSURL *)url
{
    NSString *scheme = [url.scheme lowercaseString];
    NSString *path = [url.path stringByTrimmingCharactersInSet:[NSCharacterSet punctuationCharacterSet]];
    NSString *host = url.host;
    NSInteger port = [url.port integerValue];
    if (port == 0) {
        if ([scheme isEqualToString:kHttpsScheme]) {
            port = 443;
        }
        else {
            port = 80;
        }
    }
    NSString *strippedURL = [[NSString stringWithFormat:@"%@:%@:%ld/%@", scheme, host, port ,  path] lowercaseString];
    return strippedURL;
}

#pragma mark - Actions 

- (void)cancel:(id)sender {
    [_webView stopLoading];
    NetworkActivityIndicatorNotVisible;
    if ([ABProgressHUD isVisable]) {
        [ABProgressHUD hide];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
