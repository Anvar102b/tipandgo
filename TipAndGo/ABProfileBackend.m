//
//  ABRecipientBackend.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABProfileBackend.h"
#import "ABURLSession.h"
#import "ABProfile.h"

NSString* const kProfile = @"/client/profile/";

@implementation ABProfileBackend

+ (NSURLSessionDataTask*)loadProfile:(NSString*)tipCode
                             success:(void(^)(ABProfile* profile))success
                             failure:(void(^)(ABError* error))failure {
    NSString* url = [NSString stringWithFormat:@"%@%@%@", [ABURLSession baseUrl], kProfile, tipCode];
    NSURLSessionDataTask* dataTask = [ABURLSession requestWithMethod:GET urlString:url parameters:nil success:^(id response) {
        ABProfile* profile = [[ABProfile alloc] initWithResponse:response];
        success(profile);
    } failure:^(ABError *error) {
        failure(error);
    }];
    return dataTask;
}

+ (NSURLSessionDataTask*)addPayment:(NSString*)amount
                            tipCode:(NSString*)tipCode
                            success:(void(^)())success
                            failure:(void(^)(ABError* error))failure; {
    NSString* url = [NSString stringWithFormat:@"%@%@%@/payments/add", [ABURLSession baseUrl], kProfile, tipCode];
    NSDictionary* mapData = @{@"Amount" : amount};
    NSURLSessionDataTask* dataTask = [ABURLSession requestWithMethod:POST urlString:url parameters:mapData success:^(id response) {
        
    } failure:^(ABError *error) {
        
    }];
    return dataTask;
}

@end
