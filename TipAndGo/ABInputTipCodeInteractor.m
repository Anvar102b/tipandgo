//
//  ABInputTipCodeInteractor.m
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABInputTipCodeInteractor.h"
#import "ABInputTipCodePresenter.h"

#import "ABProfileBackend.h"

#import "ABProfile.h"

@interface ABInputTipCodeInteractor ()

@property (nonatomic, strong) NSURLSessionDataTask* dataTask;

@end

@implementation ABInputTipCodeInteractor
//91510
- (void)loadProfile:(NSString *)tipCode {
    _dataTask = [ABProfileBackend loadProfile:tipCode success:^(ABProfile *profile) {
        [_presenter didLoadProfile:profile];
    } failure:^(ABError *error) {
        if (error.exceptionType == WrongCode) {
              [_presenter error:@"Профиль с введенным Tip-кодом не найден"];
        } else {
            [_presenter error:error.text];
        }      
    }];
}



@end
