//
//  AppDelegate.m
//  TipAndGo
//
//  Created by Anvar Basharov on 04.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "AppDelegate.h"
#import "ABInputTipCodeViewController.h"
#import "ABInputTipCodePresenter.h"
#import "ABInputTipCodeWireframe.h"
#import "ABInputTipCodeInteractor.h"
#import "ABPayProfileViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self makeDependencies];
    
    return YES;
}

- (void)makeDependencies {
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *nav = [mainStoryboard instantiateInitialViewController];
    ABInputTipCodeViewController *view = nav.viewControllers[0];
    ABInputTipCodePresenter* presenter = [ABInputTipCodePresenter new];
    ABInputTipCodeWireframe* wireframe = [ABInputTipCodeWireframe new];
    ABInputTipCodeInteractor* interactor = [ABInputTipCodeInteractor new];
    
    view.presenter = presenter;
    presenter.view = view;
    presenter.interactor = interactor;
    presenter.wireframe = wireframe;
    interactor.presenter = presenter;
    
    [self.window setRootViewController:nav];
    [self.window makeKeyAndVisible];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
