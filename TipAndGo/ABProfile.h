//
//  ABProfile.h
//  TipAndGo
//
//  Created by Anvar Basharov on 06.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABProfile : NSObject
/*
 {
 AvatarId = "<null>";
 Code = 91510;
 CompanyName = TestCompany;
 FirstName = Test1;
 JobTitle = "";
 LastName = Test11;
 YandexMoney = TestYandex;
 }
 
 
 */

@property (nonatomic, strong) NSURL* avatarUrl;
@property (nonatomic, strong) NSString* code;
@property (nonatomic, strong) NSString* companyName;
@property (nonatomic, strong) NSString* firstName;
@property (nonatomic, strong) NSString* lastName;
@property (nonatomic, strong) NSString* jobTitle;
@property (nonatomic, strong) NSString* yandexWallet;
@property (nonatomic, strong) UIImage* avatarImage;

- (instancetype)initWithResponse:(NSDictionary*)response;

@end
