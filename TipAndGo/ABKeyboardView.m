//
//  ABKeyboardView.m
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import "ABKeyboardView.h"

@interface ABKeyboardView ()

@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@end

@implementation ABKeyboardView

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
            UINib *nib;
        if (self.subviews.count == 0) {
            
            switch ([ABCommonHelper screenSizeInch]) {
                case ScreenSize_3_5_inch:
                case ScreenSize_4_inch:
                    nib = [UINib nibWithNibName:@"ABKeyboardViewStandart" bundle:nil];
                    break;
                default:
                    nib = [UINib nibWithNibName:@"ABKeyboardViewLarge" bundle:nil];
                    break;
            }
            UIView *subview = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            subview.frame = self.bounds;
            [self addSubview:subview];            
        }
    }
    return self;
}

#pragma mark - ABPinViewOutput

- (void)enableDelete {
    self.deleteButton.enabled = YES;
}

- (void)disableDelete {
    self.deleteButton.enabled = NO;
}

- (void)showDone {
    self.doneButton.hidden = NO;
}

- (void)hideDone {
    self.doneButton.hidden = YES;
}

#pragma mark - Private Methods

- (void)setShowDoneButton:(BOOL)showDoneButton {
    if (showDoneButton) {
        self.doneButton.hidden = NO;
    } else {
        self.doneButton.hidden = YES;
    }
}

- (IBAction)keyboardPressed:(UIButton*)sender {
    NSLog(@"sender tag %ld", (long)sender.tag);
    switch (sender.tag) {
        case 0:
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
        case 8:
        case 9:
            [_delegate didpressNum:sender.tag];
            break;
        case 10:
            [_delegate didpressDelete];
            break;
        case 11:
            if ([_delegate respondsToSelector:@selector(didPressDone)]) {
                [_delegate didPressDone];
            }
            break;
        default:
            break;
    }
    
}

@end
