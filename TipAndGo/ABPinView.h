//
//  ABPinView.h
//  TipAndGo
//
//  Created by Anvar Basharov on 05.10.16.
//  Copyright © 2016 TipAndGo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABKeyboardView.h"

@protocol ABPinViewOutput;
@protocol ABPinViewDelegate <NSObject>
- (void)didEnterTipCode:(NSString*)tipCode;
@end

@interface ABPinView : UIView <ABKeyboardViewDelegate>

@property (nonatomic, weak) id <ABPinViewDelegate> delegate;
@property (nonatomic, strong) id <ABPinViewOutput> output;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *numLabels;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *numPins;

- (void)startAnimation;
- (void)stopAnimation;
- (void)shake:(UIView*)pinView;

@end
